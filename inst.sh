#!/usr/bin/bash
# sudo bash $0
set -ex
 
abs_path=$(readlink -f $0)
path=${abs_path%/*}

#grep -vE "^$|^#" /etc/dnsmasq.conf
#conf-dir=/etc/dnsmasq.d/,*.conf
sed -i '/*.conf/ s/^#conf-dir=/conf-dir=/' /etc/dnsmasq.conf

if (! grep RestartSec= /usr/lib/systemd/system/dnsmasq.service 1>/dev/null 2>&1) then
  sed -i '/Restart=.*/ a\RestartSec=10' /usr/lib/systemd/system/dnsmasq.service
else
  sed -i 's/RestartSec=.*/RestartSec=10/' /usr/lib/systemd/system/dnsmasq.service
fi

if [ -d /etc/dnsmasq.d ]; then
  while (mount|grep /etc/dnsmasq.d >/dev/null); do umount /etc/dnsmasq.d; done
  mount -B {${path},}/etc/dnsmasq.d
else
  if [ $(readlink -f /etc/dnsmasq.d) != ${path}/etc/dnsmasq.d ];then
    ln -sf {${path},}/etc/dnsmasq.d
  fi
fi

rm -f ${path}/etc/dnsmasq.d/10-*.conf
for nic in $(ls /sys/class/net/);do
  if [ -d /sys/class/net/${nic}/device ];then
    echo ${nic}
    ipaddr=$(ip address show dev ${nic}|grep -w inet|awk '{print $2}')
    net=${ipaddr%.*}
    if (ls ${path}/etc/dnsmasq.d/inst/10-${nic}-*-${net}.*.conf 2>/dev/null)then
      ln ${path}/etc/dnsmasq.d/inst/10-${nic}-*-${net}.*.conf ${path}/etc/dnsmasq.d/
    fi
  fi
done

if [ ! -d /srv/tftp ];then
 mkdir -p /srv/tftp
fi
while (mount|grep /srv/tftp >/dev/null); do umount /srv/tftp; done

if [ $(readlink -f /srv/tftp/ipxe) != ${path}/ipxe ];then
   ln -s ${path}/ipxe /srv/tftp/
   ln -s ${path}/pxe /srv/tftp/
fi


if [ ! -d /srv/http ]; then
  mkdir -p /srv/http
fi

while (mount|grep /srv/http >/dev/null); do umount /srv/http; done

if [ $(readlink -f /srv/http/ipxe) != ${path}/ipxe ];then
   ln -s ${path}/cgi-bin /srv/http/
   ln -s ${path}/ipxe /srv/http/
   ln -s ${path}/pxe /srv/http/
   ln -s ${path}/repo /srv/http/
   ln -s ${path}/archlinux.inst /srv/http/
fi

if [ ! -f /etc/systemd/system/i-ipxe-http.service ];then
  ln -sf {${path},}/etc/systemd/system/i-ipxe-http.service
fi

if [ ! -f /etc/systemd/system/ipxe-http-repo.service ];then
  ln -sf {${path},}/etc/systemd/system/ipxe-http-repo.service
fi

systemctl daemon-reload

systemctl restart i-ipxe-http dnsmasq ipxe-http-repo
systemctl status i-ipxe-http dnsmasq ipxe-http-repo

