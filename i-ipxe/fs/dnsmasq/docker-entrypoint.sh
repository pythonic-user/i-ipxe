#! /bin/sh
set -e
echo $0: enter

echo params:
echo  http_server    =${http_server}
echo  listen_address =${listen_address}
echo  dhcp_range     =${dhcp_range}
echo  interface      =${interface}
echo  tftp_root      =${tftp_root}
echo  ipxe_nbp       =${ipxe_nbp}
echo  http_port      =${http_port}
echo  http_prefix    =${http_prefix}
echo  ipxe_script    =${ipxe_script}

src=/etc/dnsmasq.conf
src=/etc/dnsmasq.d/ipxe.conf


if (! grep ${http_server} ${src} >/dev/null 2>&1) then
    echo $0： exit
    # https://www.richud.com/wiki/Network_iPXE_dnsmasq_Examples_PXE_BOOT
    cat<<EOF>/dev/null
enable-tftp
dhcp-match=ipxe,175
dhcp-range=192.168.2.100,192.168.2.200
tftp-root=/tmp/memstick/host0_part1/
dhcp-boot=net:#ipxe,undionly.kkpxe.0
dhcp-boot=pxelinux.0
dhcp-option-force=209,pxelinux.cfg/default
dhcp-option-force=210,http://xxx.xxx.xx.xx:81/
EOF

    cat<<EOF>/dev/null
dhcp-match=ipxe,175
enable-tftp
dhcp-range=192.168.2.0,proxy
tftp-root=/pxe/
pxe-service=net:#ipxe,x86PC, "splashtop by richud.com", undionly.kkpxe
EOF

    # dhcp pxe
    cat<<EOF>>/dev/null

# for dhcp tftp (pxe)
listen-address=${listen_address}
bind-interfaces
dhcp-range=${dhcp_range:-192.168.10.200,192.168.10.250,30d}
enable-tftp
tftp-root=${tftp_root:-/tftpboot}
dhcp-match=ipxe,175
dhcp-boot=net:!ipxe,undionly.kpxe
dhcp-boot=net:ipxe,http://${http_server}:${http_port}/api/ipxe/menu/default.ipxe?sn=\${serial}&?mac=\${mac}&uuid=\${uuid}

EOF
    # proxy-dhcp pxe
    cat<<EOF>>${src}

# for dhcp tftp (pxe)
log-dhcp
port=0
listen-address=${listen_address:-127.0.0.1,192.168.56.10}
interface=${interface:enp0s3}
bind-interfaces
dhcp-range=${dhcp_range:-192.168.56.0,proxy}
enable-tftp
tftp-root=${tftp_root:-/tftpboot}
dhcp-match=ipxe,175
dhcp-boot=net:!ipxe,${ipxe_nbp:-undionly.kpxe}
dhcp-boot=net:ipxe,http://${http_server}:${http_port:-80}${http_prefix}${ipxe_script}
pxe-service=net:#ipxe,x86PC, "ipxe in local.com", ${ipxe_nbp:-undionly.kpxe}
pxe-service=net:ipxe,x86PC, "ipxe", http://${http_server}:${http_port:-80}${http_prefix}${ipxe_script}

EOF

else
    echo $0： skip
fi

exec $@
