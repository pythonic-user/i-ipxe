echo $0: enter
srcfname=/etc/nginx/conf.d/default.conf
bakfname=/etc/nginx/conf.d/default.conf.org
tmpfname=/tmp/tmp.txt

if (test -f ${bakfname}) then
    echo $0： skip
    exit 0
fi

echo '
insert multi-lines to ${srcfname} before this line:
    #error_page  404              /404.html;
'

pre_len=$(grep -n '#error_page  404' ${srcfname} |awk -F: '{print $1}')
pre_len=$(expr ${pre_len} - 1)

left_len=$(wc -l ${srcfname}|awk '{print $1}')
left_len=$(expr ${left_len} - ${pre_len})

# before
head -${pre_len} ${srcfname} > ${tmpfname}

# insert
cat<<"EOF">> ${tmpfname}
    location /api {
        root /usr/share/nginx/html;
        autoindex on;
        autoindex_format html;
        autoindex_exact_size off;
        autoindex_localtime on;
        charset utf-8;
    }

EOF

# after
tail -${left_len} ${srcfname} >>  ${tmpfname}

# backup
mv ${srcfname} ${bakfname}

# replace
mv ${tmpfname} ${srcfname}

echo $0： exit
