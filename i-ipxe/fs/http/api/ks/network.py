﻿# coding=utf-8
import time
import pprint
import traceback
import os
# local script
"""
在 ks的pre阶段产生对应的片段
"""


def load_data(_db, kwargs):
    import sims2.utils as utils2
    scheme_row = _db.Scheme.objects.get(code=int(kwargs['scheme']))
    rfdata = utils2.Utils(_db).rulefile_data(scheme_row.rulefile.code)

    sn_name = {
        i['SN']: i['NC_Name']
        for i in rfdata['sheet']['NC_SN']
    }
    name_ip = {
        i['NC_Name']: i['MGMT_IP_ADDR'].split('/')[0]
        for i in rfdata['sheet']['NC_IP_ADDR']
       }

    param = {
        'scheme': kwargs['scheme'],
        'sn': kwargs['sn'],
        'hostname': sn_name,
        'ipaddress': name_ip,
    }
    return param


def parse_script(_db, kwargs):
    from simsportal.utils import local_script
    request = kwargs.pop('request')
    assert request or True
    text = local_script(__file__, '# local script')
    param = load_data(_db, kwargs)
    param['step'] = kwargs['step']
    text = text.replace('{"data": "@@PARAM@@"}', pprint.pformat(param))
    return text

# local script


class IpxeScripts:
    """ ref
    """
    def __init__(self):
        self._param = {"data": "@@PARAM@@"}
        self._test_param = {
            'step': 'network',
            'scheme': 1,
            'sn': 'sn',
            'hostname': {'sn': 'hostname'},
            'ipaddress': {'hostname': '192.168.10.82/24'},
        }

    def main(self):
        if 'step' not in self._param:
            self._param = self._test_param
        print('step is: %(step)s' % self._param)
        text = getattr(self, self._param['step'])()
        fname = os.path.join('/tmp', 'sims_%(step)s' % self._param)
        open(fname, 'w').write(text)
        return True

    def hostname(self):
        """ 产生主机名的配置
network  --hostname=orclx82
        """
        hostname = self._param['hostname'][self._param['sn']]
        return 'network  --hostname=%(hostname)s\n' % locals()

    def network(self):
        # 产生管理网卡的配置
        templ = """
# Network information
#network  --bootproto=dhcp --device=enp0s3 --onboot=off --noipv6 --activate
network  --bootproto=static --device=enp0s8 --gateway=%s --ip=%s --netmask=255.255.255.0 --noipv6 --activate
        """
        # 172.10.2.22/24
        hostname = self._param['hostname'][self._param['sn']]
        iplen = self._param['ipaddress'][hostname]
        ipaddr = iplen.split('/')[0]
        temp = (('1'*int(iplen.split('/')[1]))+'0'*32)[:32]
        mask = '.'.join([str(int(temp[i*8:(i+1)*8], 2)) for i in range(4)])
        gateway = ''
        nic = self._mgmt_eth()
        templ = """
network  --bootproto=static --device=%(nic)s --gateway=%(gateway)s --ip=%(ipaddr)s --netmask=%(mask)s --noipv6 --activate
        """.strip() + '\n'
        return templ % locals()

    def _mgmt_eth(self):
        # 获取管理网卡名字
        nics = [
            i.strip()
            for i in os.popen('ls /sys/class/net/')
            if i.strip() and i.strip() != 'lo']
        has_ip = [
            i for i in nics
            if 'inet' in os.popen('ip address show dev %s' % i).read()]
        if len(has_ip) == 1:
            return has_ip[0]
        has_ip.sort()
        return has_ip[0]

def main():
    try:
        self = IpxeScripts()
        if self.main():
            print('%s SUCCESS' % time.strftime('%H:%M:%S'))
            return
    except BaseException:
        pprint.pprint(traceback.format_exc())
    print('%s ERROR' % time.strftime('%H:%M:%S'))


if __name__ == '__main__':
    main()
