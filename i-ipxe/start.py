# coding=utf-8
"""
1.prepare ISO_PATH
2.mount iso to repo
"""

# import sys
import os

path = os.path.dirname(
    os.path.normpath(os.path.abspath(__file__))
)

repo_path = os.path.join(path, 'fs/http/api/repo')
iso_path = os.path.join(path, 'fs/http/api/iso')
mounted = os.popen('mount|grep %s' % iso_path).read().strip()

isoinfo = [
    i.split()
    for i in """
    archlinux-2020.04.01-x86_64.iso archlinux-2020.04.01-x86_64
    archlinux-2020.07.01-x86_64.iso archlinux-2020.09.01-x86_64
    archlinux-2020.09.01-x86_64.iso archlinux-2020.07.01-x86_64

    CentOS-7-x86_64-Everything-1708.iso CentOS-7.4-1708-x86_64
    CentOS-7-x86_64-Everything-1810.iso CentOS-7.6-1810-x86_64
    CentOS-7-x86_64-Everything-2003.iso CentOS-7.8-2003-x86_64

    ubuntu-14.04.6-desktop-amd64.iso ubuntu-14.04.6-desktop-amd64
    ubuntu-16.04.6-desktop-amd64.iso ubuntu-16.04.6-desktop-amd64
    ubuntu-18.04.3-desktop-amd64.iso ubuntu-18.04.3-desktop-amd64

    alpine-standard-3.12.0-x86_64.iso alpine-standard-3.12.0-x86_64

    rhel-8.0-x86_64-dvd.iso
    rhel-server-7.4-x86_64-dvd.iso
    rhel-server-7.5-x86_64-dvd.iso
    rhel-server-7.6-x86_64-dvd.iso
    Oracle-Linux-Release-8-Update-2-for-x86-64.iso oracle_linux82
    Oracle-Linux-Release-8-Update-3-for-x86-64.iso oracle_linux83

    cn_windows_10_enterprise_2016_ltsb_x86_dvd_9057089.iso win10
""".splitlines()
    if len(i.split()) == 2
]


# search level2 iso -> raw iso path
def check_iso(iso):
    iso = os.path.realpath(iso)
    if not os.path.exists(iso):
        return
    # read
    return os.path.basename(iso), os.path.dirname(iso)


name2path = {
    i[0]: i[1]
    for i in [
        check_iso(i)
        for i in os.popen('ls /*/*.iso /*/*/*.iso').read().split()
    ]
    if i
}

paths = {}
for name, _ in isoinfo:
    if name in name2path:
        path = name2path[name]
        paths[path] = paths.get(path, 0) + 1

selectd_path = [k for k, v in paths.items() if v == max(paths.values())][0]

if not mounted:
    os.system('mount --bind %s %s' % (selectd_path, iso_path))
    print('selectd_path[%s] bind' % selectd_path)
else:
    print('selectd_path[%s] skiped' % selectd_path)

mounted_path = {
    os.path.basename(i.split()[-1])
    for i in os.popen('df -h').readlines()
    if repo_path in i
}

for name, path in isoinfo:
    iso_name = os.path.join(iso_path, name)
    repo_name = os.path.join(repo_path, path)
    if os.path.exists(iso_name) and path not in mounted_path:
        if not os.path.exists(repo_name):
            os.makedirs(repo_name)
        cmd = 'mount -o ro %s %s' % (iso_name, repo_name)
        print('mount_path[%s]' % iso_name)
        os.system(cmd)
        print(cmd)
    elif not os.path.exists(iso_name) and os.path.exists(repo_name):
        os.rmdir(repo_name)

# 修改 dnsmsq的几个配置参数
open('.env', 'w').write(
    '''# env
COMPOSE_PROJECT_NAME=i-ipxe
TZ=Asia/Shanghai
LANG=en_US.UTF-8
home=/opt/i-ipxe/

# pxe
# dhcp
dhcp_server=192.168.56.1

# http
http_server=192.168.56.10
http_port=943
http_prefix=/api

# tftp(next_server)
tftp_server=192.168.56.10
ipxe_nbp=undionly.kpxe
ipxe_script='/ipxe/menu/default.ipxe?sn=$${}{serial}&mac=$${}{mac}&uuid=$${}{uuid}'

#
#dnsmsq_listen_address=127.0.0.1,192.168.10.1
dnsmsq_listen_address=127.0.0.1,192.168.56.10
#dnsmsq_bind_interfaces=enp0s9
dnsmsq_bind_interfaces=enp0s3
#dnsmsq_dhcp_range=192.168.10.200,192.168.10.250,30d
dnsmsq_dhcp_range=192.168.56.0,proxy
dnsmsq_tftp_root=/tftpboot

    '''.strip() + '\n')
os.system('docker-compose stop;docker-compose up -d')
