#！/usr/bin/bash
# ref https://wiki.archlinux.org/title/General_recommendations

set -ex

#Contents
#1	System administration
#1.1	Users and groups
useradd -m archusr
chpasswd<<EOF_PWD
archusr:arch
EOF_PWD

#1.2	Privilege elevation
pacman --noconfirm -S sudo
usermod -aG wheel archusr
sed -i '/NOPASSWD/s/^# //g' /etc/sudoers

#1.3	Service management
#1.4	System maintenance
#2	Package management
#2.1	pacman
#2.2	Repositories
cat<<"EOF_MIR">>/etc/pacman.conf
[archlinuxcn]
Server = http://repo.archlinuxcn.org/$arch
#Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
# sudo pacman -Syy && sudo pacman -S archlinuxcn-keyring
# pacman -Syy && pacman -S archlinuxcn-keyring

[arch4edu]
Server = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch
#Include = /etc/pacman.d/mirrorlist
# pacman-key --recv-keys 7931B6D628C8D3BA
# pacman-key --finger 7931B6D628C8D3BA
# pacman-key --lsign-key 7931B6D628C8D3BA
EOF_MIR

pacman -Syu --noconfirm
#2.3	Mirrors
#2.4	Arch Build System
#2.5	Arch User Repository
#3	Booting
#3.1	Hardware auto-recognition
#3.2	Microcode
#3.3	Retaining boot messages
#3.4	Num Lock activation
#4	Graphical user interface
#4.1	Display server
#安装 软件包 xorg-server。
#此外，xorg-apps 组提供了一些程序以完成某些特定的配置工作。
#软件包组 xorg 包含了 Xorg server，xorg-apps 中的软件包以及字体.
pacman --noconfirm -S xorg
#4.2	Display drivers
#4.3	Desktop environments
pacman --noconfirm -S xfce4 xfce4-goodies
#4.4	Window managers
pacman --noconfirm -S xfwm4
#4.5	Display manager
pacman --noconfirm -S lxdm
systemctl enable lxdm
#4.6	User directories
pacman --noconfirm -S xdg-user-dirs
#5	Power management
pacman --noconfirm -S xfce4-power-manager
#5.1	ACPI events
#5.2	CPU frequency scaling
#5.3	Laptops
#5.4	Suspend and hibernate
#6	Multimedia
#6.1	Sound system
#7	Networking
for nic in $(ls /sys/class/net/|grep -v lo);do
  cat<<EOF_CONF>/etc/systemd/network/02-${nic}.network
[Match]
Name=${nic}
#[Link]
[Network]
DHCP=ipv4
#[Address]
#Address=
#[Route]
#Address=
[DHCP]
UseDNS=true
EOF_CONF
done
systemctl enable systemd-networkd systemd-resolved
pacman --noconfirm -S iwd
systemctl enable iwd
#7.1	Clock synchronization
#7.2	DNS security
#7.3	Setting up a firewall
#7.4	Network shares
pacman --noconfirm -S openssh
systemctl enable sshd
#8	Input devices
#8.1	Keyboard layouts
#8.2	Mouse buttons
#8.3	Laptop touchpads
#8.4	TrackPoints
#9	Optimization
#9.1	Benchmarking
#9.2	Improving performance
#9.3	Solid state drives
#10	System services
#10.1	File index and search
#10.2	Local mail delivery
#10.3	Printing
#11	Appearance
#11.1	Fonts
pacman --noconfirm -S wqy-microhei ttf-dejavu fcitx fcitx-im
cat<<EOF>/root/.xprofile
export LC_ALL=zh_CN.UTF-8
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
EOF
cp /root/.xprofile ~archusr/.
chown archusr:archusr ~archusr/.xprofile
#11.2	GTK and Qt themes
#12	Console improvements
#12.1	Tab-completion enhancements
#12.2	Aliases
#12.3	Alternative shells
#12.4	Bash additions
#12.5	Colored output
#12.6	Compressed files
#12.7	Console prompt
#12.8	Emacs shell
#12.9	Mouse support
#12.10	Session management
pacman --noconfirm -S virtualbox-guest-utils
systemctl enable vboxservice

