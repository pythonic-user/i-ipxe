#！/usr/bin/bash
set -ex

DEV_SDA=/dev/$(lsblk|grep -vE "NAME|loop|archiso"|awk '{print $1}')
HOSTNAME=arch-2022
PASSWD=arch

# ref https://wiki.archlinux.org/title/Installation_guide

# Pre-installation
## Verify the boot mode
ls /sys/firmware/efi/efivars && echo uefi || echo bois

## Connect to the internet
ip link
ping -c 4 archlinux.org

## Update the system clock
timedatectl set-ntp true

## Partition the disks
fdisk -l
fdisk -l|grep ${DEV_SDA} >/dev/null 2>&1
wipefs -a ${DEV_SDA}
parted ${DEV_SDA} mklabel msdos
parted ${DEV_SDA} mkpart primary xfs 1M 100%
parted ${DEV_SDA} set 1 boot on

## Format the partitions
mkfs.xfs -f ${DEV_SDA}1

## Mount the file systems
mount ${DEV_SDA}1 /mnt


#Installation

## Select the mirrors
curl -o /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=CN"
sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist

## Install essential packages
pacstrap /mnt base linux linux-firmware

# Configure the system

## Fstab
genfstab -U /mnt >> /mnt/etc/fstab

## Chroot
#>arch-chroot /mnt
cat<<EOF_CHROOT>/mnt/chroot.bash
#！/usr/bin/bash
set -ex

## Time zone
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc

## Localization
sed -i 's/^#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
sed -i 's/^#zh_CN.UTF-8/zh_CN.UTF-8/g' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >/etc/locale.conf
#echo "KEYMAP=de-latin1" >/etc/vconsole.conf

## Network configuration
echo ${HOSTNAME} >/etc/hostname

## Initramfs
pacman --noconfirm -S xfsprogs
mkinitcpio -P

## Root password
# passwd
chpasswd<<EOF_PWD
root:${PASSWD}
EOF_PWD

mkdir -p /root/.ssh
cat<<EOF_AUTH>/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF_AUTH
chmod 0700 /root/.ssh
chmod 0600 /root/.ssh/authorized_keys

## Boot loader
pacman --noconfirm -S grub
grub-install --target=i386-pc --recheck ${DEV_SDA}
grub-mkconfig -o /boot/grub/grub.cfg

EOF_CHROOT

arch-chroot /mnt bash /chroot.bash

# Post-installation
cat<<"EOF_POST">/mnt/post.bash
#！/usr/bin/bash
set -ex

# network
for nic in $(ls /sys/class/net/|grep -v lo);do
  cat<<EOF_CONF>/etc/systemd/network/02-${nic}.network
[Match]
Name=${nic}
#[Link]
[Network]
DHCP=ipv4
#[Address]
#Address=
#[Route]
#Address=
[DHCP]
UseDNS=true
EOF_CONF
done
systemctl enable systemd-networkd systemd-resolved

#
cat<<"EOF_MIR">>/etc/pacman.conf
[archlinuxcn]
Server = http://repo.archlinuxcn.org/$arch
#Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
# sudo pacman -Syy && sudo pacman -S archlinuxcn-keyring
# pacman -Syy && pacman -S archlinuxcn-keyring

[arch4edu]
Server = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch
#Include = /etc/pacman.d/mirrorlist
# pacman-key --recv-keys 7931B6D628C8D3BA
# pacman-key --finger 7931B6D628C8D3BA
# pacman-key --lsign-key 7931B6D628C8D3BA
EOF_MIR

pacman -Syu --noconfirm


pacman --noconfirm -S iwd iw openssh
systemctl enable iwd openssh

# 图形用户界面 KDE
pacman --noconfirm -S xorg xorg-apps
pacman --noconfirm -S plasma kde-applications
pacman --noconfirm -S sddm
pacman --noconfirm -S kcm-fcitx

pacman --noconfirm -S wqy-microhei ttf-dejavu fcitx fcitx-im
cat<<EOF_INPUT>~/.pam_environment
GTK_IM_MODULE=fcitx
QT_IM_MODULE=fcitx
XMODIFIERS=@im=fcitx
EOF_INPUT

# 图形用户界面 XFCE
#pacman --noconfirm -S xfce4 xfce4-goodies lxdm
#systemctl enable lxdm

## 中文字体，输入法
#pacman --noconfirm -S wqy-microhei ttf-dejavu fcitx fcitx-im
#cat<<EOF>/root/.xprofile
#export LC_ALL=zh_CN.UTF-8
#export GTK_IM_MODULE=fcitx
#export QT_IM_MODULE=fcitx
#export XMODIFIERS="@im=fcitx"
#EOF

pacman --noconfirm -S virtualbox-guest-utils
systemctl enable vboxservice

EOF_POST

#arch-chroot /mnt bash /post.bash

echo umount /mnt
echo reboot

