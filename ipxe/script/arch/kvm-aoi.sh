#！/usr/bin/bash
set -ex
# kvm中archlinux离线安装（只有ISO）
# 主要流程
# https://wiki.archlinux.org/index.php/Installation_guide
SERVER=192.168.1.27
DEV_SDA=/dev/vda
HOSTNAME=arch-zero
PASSWD=archpwd
IPADDR=192.168.1.107

#分区, 只分一个,选dos
echo parted ...
wipefs -a ${DEV_SDA}
parted ${DEV_SDA} mklabel msdos
parted ${DEV_SDA} mkpart primary xfs 1M 100%
parted ${DEV_SDA} set 1 boot on


#文件系统
echo filesystem ...
mkfs.xfs -f ${DEV_SDA}1
mount ${DEV_SDA}1 /mnt

# 离线安装
# https://wiki.archlinux.org/index.php/Offline_installation
echo offline inst file ...
cp -ax / /mnt

if [ -d /run/archiso/bootmnt/arch/boot ];then
  cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz* /mnt/boot/vmlinuz-linux
else
  curl -o /mnt/boot/vmlinuz-linux http://${SERVER}/repo/archlinux-2020.04.01-$(uname -m)/arch/boot/x86_64/vmlinuz
fi

# 进入新安装的环境执行
cat<<"EOF_SH">/mnt/root/offline_set.sh
#！/usr/bin/bash
set -ex
echo Restore the configuration of journald ...
sed -i 's/Storage=volatile/#Storage=auto/' /etc/systemd/journald.conf
echo Remove special udev rule...
rm -f /etc/udev/rules.d/81-dhcpcd.rules
echo Disable and remove the services created by archiso...
systemctl disable pacman-init.service choose-mirror.service
rm -f -r /etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm -f /etc/systemd/scripts/choose-mirror
echo Remove special scripts of the Live environment ...
rm -f /etc/systemd/system/getty@tty1.service.d/autologin.conf
rm -f /root/{.automated_script.sh,.zlogin}
rm -f /etc/mkinitcpio-archiso.conf
rm -f -r /etc/initcpio
echo Importing archlinux keys...
pacman-key --init
pacman-key --populate archlinux
EOF_SH

arch-chroot /mnt /bin/bash /root/offline_set.sh
###############################################################################

#安装完成后执行
echo genfstab ...
genfstab -U /mnt >> /mnt/etc/fstab

# 下面在新环境执行
cat<<"EOF_SH">/mnt/root/comm_set.sh
#！/usr/bin/bash
set -ex
echo localtime ...
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#同步硬件时钟
echo hwclock ...
hwclock -w
EOF_SH

cat<<EOF_SH>>/mnt/root/comm_set.sh
echo hostname ...
echo ${HOSTNAME:-arch-host} > /etc/hostname
EOF_SH

# 编辑本地化配置文件
cat<<"EOF_SH">>/mnt/root/comm_set.sh
echo locale-gen ...
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /etc/locale.gen
locale-gen

echo locale.conf ...
echo LANG=en_US.UTF-8 > /etc/locale.conf
EOF_SH

cat<<EOF_SH>>/mnt/root/comm_set.sh

echo ${HOSTNAME} > /etc/hostname

# Initramfs
echo Initramfs ...
mkinitcpio -P
#
#arch-chroot /mnt pacman --noconfirm -S grub
grub-install --target=i386-pc --recheck ${DEV_SDA}
grub-mkconfig -o /boot/grub/grub.cfg


# Root password
chpasswd<<EOP
root:${PASSWD}
EOP

mkdir -p /root/.ssh
cat<<EOF>/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF
chmod 0700 /root/.ssh
chmod 0600 /root/.ssh/authorized_keys
EOF_SH


cat<<EOF_SH>>/mnt/root/comm_set.sh
# 配置网络，设置静态ip
cat<<EOF>/etc/systemd/network/01-enp1s0.network
[Match]
Name=enp1s0

[Network]
Address=${IPADDR}
#Gateway=192.168.1.1
#DNS=8.8.8.8
EOF
systemctl enable systemd-networkd

# 开启ssh登录服务
systemctl enable sshd

# 重新启动
umount /mnt
echo reboot
EOF_SH
cp $0 /mnt/root
arch-chroot /mnt /bin/bash /root/comm_set.sh

