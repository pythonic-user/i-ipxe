#！/usr/bin/bash
# from 2020-04-01 offline inst

set -ex

curl -o /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=CN"
sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist

rm /usr/lib/p11-kit-trust.so
pacman -Sy archlinux-keyring --noconfirm

rm /usr/lib/p11-kit-trust.so
pacman -Syu --noconfirm

cat<<"EOF">>/etc/pacman.conf
[archlinuxcn]
Server = http://repo.archlinuxcn.org/$arch
#Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
# sudo pacman -Syy && sudo pacman -S archlinuxcn-keyring
# pacman -Syy && pacman -S archlinuxcn-keyring

[arch4edu]
Server = https://mirrors.tuna.tsinghua.edu.cn/arch4edu/$arch
#Include = /etc/pacman.d/mirrorlist
# pacman-key --recv-keys 7931B6D628C8D3BA
# pacman-key --finger 7931B6D628C8D3BA
# pacman-key --lsign-key 7931B6D628C8D3BA
EOF

pacman -Syu --noconfirm

# 图形用户界面
pacman --noconfirm -S xfce4 xfce4-goodies lxdm
systemctl enable lxdm

# 中文字体，输入法
pacman --noconfirm -S wqy-microhei ttf-dejavu fcitx fcitx-im
cat<<EOF>/root/.xprofile
export LC_ALL=zh_CN.UTF-8
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
EOF

pacman --noconfirm -S virtualbox-guest-utils
systemctl enable vboxservice


