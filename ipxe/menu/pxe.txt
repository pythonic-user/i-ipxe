#!ipxe
echo Booting to pxe
set 210:string tftp://${proxydhcp/dhcp-server}/
set 209:string pxe/pxelinux.cfg/default
#set 208:hex f1:00:74:7e
set filename ${210:string}pxe/pxelinux.0
chain ${filename} ||reboot
