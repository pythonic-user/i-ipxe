# coding=utf-8

"""
# 准备源
curl -o /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=CN"
sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist

#分区, 只分一个,选dos
parted /dev/sda mklabel msdos
parted /dev/sda mkpart primary xfs 1M 100%
parted /dev/sda set 1 boot on


#文件系统
mkfs.xfs /dev/sda1
mount /dev/sda1 /mnt

# 安装
pacstrap /mnt base linux linux-firmware

#安装完成后执行
genfstab -U /mnt >> /mnt/etc/fstab

cp /usr/bin/genfstab /mnt/
cp $0 /mnt/

# 进入新安装的环境执行
#arch-chroot /mnt

#
#pacman -S bash-completion nano
arch-chroot /mnt pacman --noconfirm -S\
  pacman-contrib sudo xfsprogs expect\
  grub openssh vi dhcpcd

# 下面在新环境执行
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#同步硬件时钟
arch-chroot /mnt hwclock -w

echo arch-host > /mnt/etc/hostname

# 编辑本地化配置文件
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

#arch-chroot /mnt echo LANG=en_US.UTF-8 > /etc/locale.conf # shell xxx
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf
sed -i 's/# %wheel/%wheel/g' /mnt/etc/sudoers

arch-chroot /mnt useradd -m -G wheel -s /bin/bash arch-dev
arch-chroot /mnt expect <<EOCMD
spawn passwd
while 1 {
 expect {
  "assword:" {
   send "archpwd\n"
  }
  eof {
   exit
  }
 }
}
EOCMD

arch-chroot /mnt expect <<EOCMD
spawn passwd arch-dev
while 1 {
 expect {
  "assword:" {
   send "archdevpwd\n"
  }
  eof {
   exit
  }
 }
}
EOCMD

#安装 GRUB 软件包
#arch-chroot /mnt pacman --noconfirm -S grub
arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg



"""
import sys
import os
import time
import argparse


class InstallArchlinux:
    """
安装archlinux:
    python arch_inst.py

    export HOSTNAME=xxx
    export NIC=enp0s3:dhcp;enp0s3:192.168.56.101/24:192.168.56.1:192.168.56.1;
    export TYPE=vbox-serv/vbox-desktop/host-desktop/host-serv
    python arch_inst.py

输出：
    install.sh
    chroot.sh

    """
    def __init__(self):
        self.args = None

    def _parser_args(self):
        parser = argparse.ArgumentParser(
            prog='install_arch.py',
            description='Install archlinux 2020.04.01.',
            epilog='at last use: python %(prog)s ...|bash')
        parser.add_argument(
            '--type', metavar='T', type=str,
            help='vbox-serv/vbox-desktop/host-desktop/host-serv')
        parser.add_argument(
            '--install', metavar='I', type=str, default='online',
            help='online/offline')
        parser.add_argument(
            '--hostname', metavar='H', type=str,
            help='hostname')
        parser.add_argument(
            '--nic', metavar='N', type=str, default='',
            help='enp0s3:dhcp;enp0s3:192.168.56.101/24:192.168.56.1:192.168.56.1;')

        self.args = parser.parse_args()
        if self.args.command not in (
                'serv', 'desktop'):
            parser.print_help()
            sys.exit(-1)

    def _exec(self, cmd):
        print(
            time.strftime('%H:%M:%S'), '>>', cmd.rstrip('.'), '...',
            end=' ', flush=True)
        btime = time.time()
        res = os.system(cmd)
        dlt = time.time() - btime
        print(dlt, 'ok' if not res else 'error')
        if res != 0:
            sys.exit(-1)

    def _parsecmd(self, text):
        cmd, end = [], None
        for line in text.splitlines():
            if not end:
                if not line.strip() or line.strip()[0] == '#':
                    continue
                if '<<' in line:
                    end = line.split('<<')[1].split('>')[0].strip()
                    cmd = [line]
                    print('end', end)
                else:
                    yield line.strip()
            else:
                cmd.append(line)
                if line.startswith(end):
                    yield '\n'.join(cmd)
                    cmd, end = [], None
        if cmd:
            print(cmd)
            assert False

    def main(self):
        self._parser_args()
        gen_install()
        gen_chroot()

    def gen_install(self):
        steps = []
        if self.args.type.startswith('vbox'):
            steps.append(self._vbox_fs)
        else:
            # steps.append(self._host_fs)
            pass
        if self.args.install == 'online':
            steps.append(self._online)
        else:
            steps.append(self._offline)

        with open('install.sh', 'w') as ofs:
            for step in steps:
                print('echo', step.__name__, '='*32)
                for cmd in self._parsecmd(step()):
                    ofs.write(cmd.strip()+'\n')

    def gen_chroot(self):
        with open('chroot.sh', 'w') as ofs:
            for cmd in self._parsecmd(self._conf_base()):
                ofs.write(cmd.strip()+'\n')
            if self.args.type.startswith('vbox-'):
                for cmd in self._parsecmd(self._vboxext()):
                    ofs.write(cmd.strip()+'\n')
            for nicinfo in self.args.nic.split(';'):
                nicinfo = nicinfo.strip()
                if not nicinfo:
                    continue
                fields = nicinfo.split(':')
                if len(fields) == 2:
                    # assert fields[1] == 'dhcp'
                    pass

    def _vbox_fs(self):
        # 分区, 文件系统(只分一个,选dos)
        return """
echo parted ...
parted /dev/sda mklabel msdos
parted /dev/sda mkpart primary xfs 1M 100%
parted /dev/sda set 1 boot on

echo filesystem ...
mkfs.xfs /dev/sda1
mount /dev/sda1 /mnt
"""
    def _online(self):
        """准备源 在线安装"""
        return """
echo prepare mirrorlist
curl -o /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=CN"
sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist

pacstrap /mnt base linux linux-firmware
"""

    def _offline(self):
        # 离线安装
        # https://wiki.archlinux.org/index.php/Offline_installation
        return """
echo offline inst file ...
cp -ax / /mnt
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz* /mnt/boot/vmlinuz-linux
# 进入新安装的环境执行
#arch-chroot /mnt
#arch-chroot /mnt /bin/bash

echo Restore the configuration of journald ...
sed -i 's/Storage=volatile/#Storage=auto/' /mnt/etc/systemd/journald.conf
echo Remove special udev rule...
rm -f /mnt/etc/udev/rules.d/81-dhcpcd.rules
echo Disable and remove the services created by archiso...
arch-chroot /mnt systemctl disable pacman-init.service choose-mirror.service
rm -rf /mnt/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm -f /mnt/etc/systemd/scripts/choose-mirror
echo Remove special scripts of the Live environment ...
rm -f /mnt/etc/systemd/system/getty@tty1.service.d/autologin.conf
rm -f /mnt/root/{.automated_script.sh,.zlogin}
rm -f /mnt/etc/mkinitcpio-archiso.conf
rm -rf /mnt/etc/initcpio
echo Importing archlinux keys...
arch-chroot /mnt pacman-key --init
arch-chroot /mnt pacman-key --populate archlinux
###############################################################################
"""

    def _conf_base(self):
        # 安装完成后执行 fstab localtime 同步硬件时钟 编辑本地化配置文件
        #
        return """
echo genfstab ...
genfstab -U /mnt >> /mnt/etc/fstab
cp /usr/bin/genfstab /mnt
cp arch_install.py /mnt

echo localtime ...
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

echo hwclock ...
arch-chroot /mnt hwclock -w

echo locale-gen ...
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

#arch-chroot /mnt echo LANG=en_US.UTF-8 > /etc/locale.conf # shell xxx
echo locale.conf ...
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf

# Initramfs
echo Initramfs ...
arch-chroot /mnt mkinitcpio -P
#安装 GRUB 软件包 只有上面不能正常引导
#arch-chroot /mnt pacman --noconfirm -S grub
arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
"""

    def _conf_param(self):
        # hostname rootpwd
        # HOST_NAME
        # ROOT_PWD
        # nic:ip:gw:dns
        # NIC=(enp0s3:192.168.56.27:192.168.56.1:)
        return """
arch-chroot /mnt pacman --noconfirm -S expect
echo hostname ...
echo %s > /mnt/etc/hostname

# Root password
echo arch-chroot /mnt passwd ！！
expect << EOF
spawn passwd
while 1 {
    expect {

        "New password:" {
            send "${ROOT_PWD}\n"
        }
        "Retype new password:" {
            send "${ROOT_PWD}\n"
        }
        eof {
            exit
        }
    }
}
EOF

mkdir -p /mnt/root/.ssh

cat<<EOF>/mnt/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF

chmod 0700 /mnt/root/.ssh
chmod 0600 /mnt/root/.ssh/authorized_keys
"""

    def _network(self):
        return """
# 配置网络，设置静态ip
echo config network
cat<<EOF>>/mnt/etc/dhcpcd.conf

interface %s
static ip_address=%s
static routers=192.168.56.1
static domain_name_servers=192.168.56.1

EOF
arch-chroot /mnt systemctl enable dhcpcd@%s
""" % (self.args.netif, self.args.ipaddr, self.args.netif)

    def _pacman(self):
        return """
arch-chroot /mnt pacman --noconfirm -S pacman-contrib sudo xfsprogs expect grub openssh vi dhcpcd

"""

    def _sshd(self):
        # 开启ssh登录服务
        return """
arch-chroot /mnt systemctl enable sshd
"""

    def _reboot(self):
        return """
# 重新启动
echo umount /mnt
echo reboot
"""

    def _ntp(self):
        return """
arch-chroot /mnt pacman --noconfirm -S ntp

cat<<EOF>/mnt/etc/ntp.conf
server 127.127.1.0
fudge  127.127.1.0 stratum 10

restrict default kod nomodify notrap nopeer noquery
restrict -6 default kod nomodify notrap nopeer noquery

restrict 127.0.0.1
restrict -6 ::1

driftfile /var/lib/ntp/ntp.drift
logfile /var/log/ntp.log
EOF
arch-chroot /mnt systemctl enable ntpd
"""

    def _nfs(self):
        return """
arch-chroot /mnt pacman --noconfirm -S nfs-utils
arch-chroot /mnt mkdir -p /srv/nfs/
arch-chroot /mnt cat<<EOF>>/etc/fstab
#/var/lib/tftpboot /srv/nfs/tftpboot  none   bind   0   0
EOF

arch-chroot /mnt grep -v -E "^#|^$" /etc/nfs.conf
arch-chroot /mnt sed -i '/^\[nfsd\]$/a\host=192.168.56.%s' /etc/nfs.conf

arch-chroot /mnt cat<<EOF>/etc/exports.d/srv.exports
/srv/nfs        192.168.56.0/24(rw,sync,crossmnt,fsid=0)
EOF

arch-chroot /mnt systemctl enable nfs-server.service

""" % (self.args.ipaddr)
    def _passwd(self):
        return """
arch-chroot /mnt expect <<EOCMD
spawn passwd
while 1 {
 expect {
  "assword:" {
   send "archpwd\n"
  }
  eof {
   exit
  }
 }
}
EOCMD
"""
    def _vboxext(self):
        # 安装VBOX扩展
        return """
arch-chroot /mnt pacman --noconfirm -S virtualbox-guest-utils-nox
arch-chroot /mnt systemctl enable vboxservice
"""
    def _samba(self):
        return """
arch-chroot /mnt pacman --noconfirm -S samba
arch-chroot /mnt systemctl enable smb

"""

    def _iptables(self):
        # https://wiki.archlinux.org/index.php/Iptables_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)
        return ""

    def _dnsmasq(self):
        return """
arch-chroot /mnt pacman --noconfirm -S dnsmasq

sed -i '/#conf-dir=.*\*.conf/a\conf-dir=\/etc\/dnsmasq.d,*.conf' /mnt/etc/dnsmasq.conf

cat<<EOF>/mnt/etc/dnsmasq.d/dns.conf
log-dhcp
#port=0
bogus-priv
domain-needed
no-poll
resolv-file=/etc/dnsmasq.d/resolv.conf.0
addn-hosts=/etc/dnsmasq.d/hosts.0

log-queries
log-facility=/var/log/dnsmasq1.log
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/dhcp-enp0s8.conf
listen-address=192.168.56.27,127.0.0.1
##dhcp-host=192.168.56.27
#interface=enp0s8
dhcp-range=192.168.56.0,proxy
##dhcp-option=3,192.168.56.27
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/dhcp-enp0s9.conf
listen-address=192.168.10.27
#interface=enp0s9
#dhcp-host=192.168.10.27
dhcp-range=192.168.10.100,192.168.10.200,1h
#dhcp-option=3,192.168.10.27
#08:00:27:d7:ef:94
dhcp-host=08:00:27:D7:EF:94,192.168.10.127,infinite
dhcp-host=08:00:27:83:93:2F,192.168.10.10,infinite
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/tftp.conf
enable-tftp
tftp-root=/srv/tftpboot
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/dhcp.conf

bind-interfaces
dhcp-match=ipxe,175
dhcp-boot=net:!ipxe,undionly.kpxe
dhcp-boot=net:ipxe,http://${next-server}/ipxe/menu/main.txt
pxe-service=net:#ipxe,x86PC, "ipxe in local.com", undionly.kpxe
pxe-service=net:ipxe,x86PC, "ipxe", http://${next-server}/ipxe/menu/main.txt
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/resolv.conf.0
nameserver 114.114.114.114
nameserver 8.8.8.8
EOF

cat<<EOF>/mnt/etc/dnsmasq.d/hosts.0
# Static table lookup for hostnames.
# See hosts(5) for details.
192.168.10.127 arch-dev
192.168.10.27 arch-host
192.168.10.10 arch-serv
10.45.69.11 sims11
10.45.11.100 sims100
10.45.10.194 sims194
EOF

cat<<EOF>/mnt/etc/resolv.conf.head
# self
nameserver 127.0.0.1
EOF

"""


if __name__ == '__main__':
    self = InstallArchlinux()
    self.main()
