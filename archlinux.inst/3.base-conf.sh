#！/usr/bin/bash
set -ex

echo genfstab ...
genfstab -U /mnt >> /mnt/etc/fstab

echo localtime ...
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

echo hwclock ...
arch-chroot /mnt hwclock -w

echo hostname ...
echo arch-host > /mnt/etc/hostname

echo locale-gen ...
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

echo locale.conf ...
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf

echo Initramfs ...
arch-chroot /mnt mkinitcpio -P
arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

echo arch-chroot /mnt passwd ！！
arch-chroot /mnt pacman --noconfirm -S expect
arch-chroot /mnt expect <<EOCMD
spawn passwd
while 1 {
 expect {
  "assword:" {
   send "archpwd\n"
  }
  eof {
   exit
  }
 }
}
EOCMD

arch-chroot /mnt -p /root/.ssh
arch-chroot /mnt touch /root/.ssh/authorized_keys
cat<<EOF>>/mnt/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF
arch-chroot /mnt chmod 0700 /root/.ssh
arch-chroot /mnt chmod 0600 /root/.ssh/authorized_keys
