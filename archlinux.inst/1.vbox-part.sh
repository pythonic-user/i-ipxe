#！/usr/bin/bash
set -ex

echo parted ...
parted /dev/sda mklabel msdos
parted /dev/sda mkpart primary xfs 1M 100%
parted /dev/sda set 1 boot on

echo filesystem ...
mkfs.xfs /dev/sda1
mount /dev/sda1 /mnt
