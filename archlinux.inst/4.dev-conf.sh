#！/usr/bin/bash
set -ex

sed -i 's/# %wheel/%wheel/g' /mnt/etc/sudoers

arch-chroot /mnt useradd -m -G wheel -s /bin/bash arch-dev
arch-chroot /mnt expect <<EOCMD
spawn passwd arch-dev
while 1 {
 expect {
  "assword:" {
   send "archdevpwd\n"
  }
  eof {
   exit
  }
 }
}
EOCMD

#启用网络管理器
arch-chroot /mnt pacman --noconfirm -S networkmanager virtualbox-guest-utils
arch-chroot /mnt systemctl enable --now NetworkManager
#禁用网络管理器的连接等待
arch-chroot /mnt systemctl disable NetworkManager-wait-online
#使用 SSH
#arch-chroot /mnt pacman --noconfirm -S openssh
arch-chroot /mnt systemctl enable sshd
#启用 NTP 同步时间服务
arch-chroot /mnt timedatectl set-ntp true
# 每周自动清理不需要的 Pacman 软件包缓存
arch-chroot /mnt systemctl enable paccache.timer


# 图形用户界面
arch-chroot /mnt pacman --noconfirm -S xfce4 xfce4-goodies
arch-chroot /mnt pacman --noconfirm -S lxdm
arch-chroot /mnt systemctl enable lxdm

# 中文字体，输入法
arch-chroot /mnt pacman --noconfirm -S wqy-microhei ttf-dejavu
arch-chroot /mnt pacman --noconfirm -S fcitx fcitx-im
cat<<EOF>/mnt/root/.xprofile
export LC_ALL=zh_CN.UTF-8
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
EOF
cp /mnt{/root,/home/arch-dev}/.xprofile
