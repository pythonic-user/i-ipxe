#！/usr/bin/bash
set -ex

#启用网络管理器
arch-chroot /mnt pacman --noconfirm -S dhcpcd
arch-chroot /mnt systemctl enable dhcpcd@enp0s3
arch-chroot /mnt systemctl enable dhcpcd@enp0s8
arch-chroot /mnt systemctl enable dhcpcd@enp0s9

cat<<EOF>>/mnt/etc/dhcpcd.conf

interface enp0s9
static ip_address=192.168.110.1/24
static routers=192.168.110.1
static domain_name_servers=192.168.110.1
EOF


#使用 SSH
arch-chroot /mnt pacman --noconfirm -S openssh
arch-chroot /mnt systemctl enable sshd

#启用 NTP 同步时间服务
arch-chroot /mnt timedatectl set-ntp true

# NFS
arch-chroot /mnt pacman --noconfirm -S nfs-utils
cat<<EOF>mnt/etc/exports.d/nfs.exports
/srv/nfs 192.168.0.0/16(rw,sync,nohide)
EOF
