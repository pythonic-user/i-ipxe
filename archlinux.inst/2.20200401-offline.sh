#！/usr/bin/bash
set -ex

echo offline inst file ...
cp -ax / /mnt
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz /mnt/boot/vmlinuz-linux

echo Restore the configuration of journald ...
sed -i 's/Storage=volatile/#Storage=auto/' /mnt/etc/systemd/journald.conf

echo Remove special udev rule...
rm /mnt/etc/udev/rules.d/81-dhcpcd.rules

echo Disable and remove the services created by archiso...
arch-chroot /mnt systemctl disable pacman-init.service choose-mirror.service
rm -r /mnt/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm /mnt/etc/systemd/scripts/choose-mirror

echo Remove special scripts of the Live environment ...
rm /mnt/etc/systemd/system/getty@tty1.service.d/autologin.conf
rm /mnt/root/{.automated_script.sh,.zlogin}
rm /mnt/etc/mkinitcpio-archiso.conf
rm -r /mnt/etc/initcpio

echo Importing archlinux keys...
arch-chroot /mnt pacman-key --init
arch-chroot /mnt pacman-key --populate archlinux
