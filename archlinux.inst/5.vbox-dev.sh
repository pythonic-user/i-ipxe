#！/usr/bin/bash
set -ex

arch-chroot /mnt pacman --noconfirm -S virtualbox-guest-utils
arch-chroot /mnt systemctl enable vboxservice
