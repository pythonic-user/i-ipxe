#！/usr/bin/bash
set -ex

# 准备源
curl -o /etc/pacman.d/mirrorlist "https://archlinux.org/mirrorlist/?country=CN"
sed -i 's/#Server/Server/g' /etc/pacman.d/mirrorlist

# 安装
pacstrap /mnt base linux linux-firmware

#
mv /mnt/etc/pacman.d/mirrorlist{,.$(date +%Y%m%d)}
cp {,/mnt}/etc/pacman.d/mirrorlist
arch-chroot /mnt pacman -Sy
