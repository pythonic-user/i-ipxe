# coding=utf-8

"""
#！/usr/bin/bash

# vbox中archlinux离线安装（只有ISO）
# 主要流程
# https://wiki.archlinux.org/index.php/Installation_guide

#分区, 只分一个,选dos
echo parted ...
parted /dev/sda mklabel msdos
parted /dev/sda mkpart primary xfs 1M 100%
parted /dev/sda set 1 boot on


#文件系统
echo filesystem ...
mkfs.xfs /dev/sda1
mount /dev/sda1 /mnt

# 离线安装
# https://wiki.archlinux.org/index.php/Offline_installation
###############################################################################
echo offline inst file ...
cp -ax / /mnt
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz /mnt/boot/vmlinuz-linux
# 进入新安装的环境执行
#arch-chroot /mnt
#arch-chroot /mnt /bin/bash

echo Restore the configuration of journald ...
sed -i 's/Storage=volatile/#Storage=auto/' /mnt/etc/systemd/journald.conf
echo Remove special udev rule...
rm /mnt/etc/udev/rules.d/81-dhcpcd.rules
echo Disable and remove the services created by archiso...
arch-chroot /mnt systemctl disable pacman-init.service choose-mirror.service
rm -r /mnt/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm /mnt/etc/systemd/scripts/choose-mirror
echo Remove special scripts of the Live environment ...
rm /mnt/etc/systemd/system/getty@tty1.service.d/autologin.conf
rm /mnt/root/{.automated_script.sh,.zlogin}
rm /mnt/etc/mkinitcpio-archiso.conf
rm -r /mnt/etc/initcpio
echo Importing archlinux keys...
arch-chroot /mnt pacman-key --init
arch-chroot /mnt pacman-key --populate archlinux
###############################################################################

#安装完成后执行
echo genfstab ...
genfstab -U /mnt >> /mnt/etc/fstab

# 下面在新环境执行
echo localtime ...
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

#同步硬件时钟
echo hwclock ...
arch-chroot /mnt hwclock -w

echo hostname ...
echo ${HOSTNAME:-arch-host} > /mnt/etc/hostname

# 编辑本地化配置文件
echo locale-gen ...
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

#arch-chroot /mnt echo LANG=en_US.UTF-8 > /etc/locale.conf # shell xxx
echo locale.conf ...
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf

# /etc/hosts

# Initramfs
echo Initramfs ...
arch-chroot /mnt mkinitcpio -P
#安装 GRUB 软件包 只有上面不能正常引导
#arch-chroot /mnt pacman --noconfirm -S grub
arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

# Root password
echo arch-chroot /mnt passwd ！！
arch-chroot /mnt -p /root/.ssh
arch-chroot /mnt cat<<EOF>>/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF
arch-chroot /mnt chmod 0700 /root/.ssh
arch-chroot /mnt chmod 0600 /root/.ssh/authorized_keys

# 配置网络，设置静态ip
echo config network
arch-chroot /mnt cat<<EOF>>/etc/dhcpcd.conf

interface enp0s8
static ip_address=192.168.56.${IPADDRESS:-27}
static routers=192.168.56.1
static domain_name_servers=192.168.56.1

EOF
arch-chroot /mnt systemctl enable dhcpcd@enp0s8

# 开启ssh登录服务
arch-chroot /mnt systemctl enable sshd


# 重新启动
echo umount /mnt
echo reboot
"""
import sys
import os
import time
import argparse


class InstallArchlinux:
    """ 在vbox vm上面安装archlinux 离线安装，无网络有ISO """
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Install archlinux 2020.04.01.')
        parser.add_argument(
            'command', metavar='CMD', type=str,
            help='cmd: install, test')
        parser.add_argument(
            '--hostname', metavar='HOST', type=str, default='arch-host',
            help='hint hostname default arch-host')
        parser.add_argument(
            '--netif', metavar='ETH', type=str, default='enp0s8',
            help='hint network interface default enp0s8')
        parser.add_argument(
            '--ipaddr', metavar='IP', type=str, default='192.168.56.27',
            help='hint ip-address 192.168.56.27')

        self.args = parser.parse_args()
        if self.args.command not in (
                'install', 'test'):
            parser.print_help()
            sys.exit(-1)

    def _exec(self, cmd):
        print(
            time.strftime('%H:%M:%S'), '>>', cmd.rstrip('.'), '...',
            end=' ', flush=True)
        btime = time.time()
        res = os.system(cmd)
        dlt = time.time() - btime
        print(dlt, 'ok' if not res else 'error')
        if res != 0:
            sys.exit(-1)

    def _parsecmd(self, text):
        cmd, end = [], None
        for line in text.splitlines():
            if not end:
                if not line.strip() or line.strip()[0] == '#':
                    continue
                if '<<' in line:
                    end = line.split('<<')[1].split('>')[0].strip()
                    cmd = [line]
                    print('end', end)
                else:
                    yield line.strip()
            else:
                cmd.append(line)
                if line.rstrip() == end:
                    yield '\n'.join(cmd)
                    cmd, end = [], None
        if cmd:
            print(cmd)
            assert False

    def main(self):
        steps = [
            self._parted,
            self._fs,
            self._offline,
            self._fstab,
            self._localtime,
            self._hwclock,
            self._hostname,
            self._locale,
            self._initramfs,
            self._root,
            self._network,
            self._sshd,
            self._reboot,
        ]
        for step in steps:
            print('echo', step.__name__, '='*32)
            for cmd in self._parsecmd(step()):
                if self.args.command == 'install':
                    self._exec(cmd)
                elif self.args.command == 'test':
                    print(cmd)

    def _parted(self):
        # 分区, 只分一个,选dos
        return """
echo parted ...
parted /dev/sda mklabel msdos
parted /dev/sda mkpart primary xfs 1M 100%
parted /dev/sda set 1 boot on
"""

    def _fs(self):
        # 文件系统
        return """
echo filesystem ...
mkfs.xfs /dev/sda1
mount /dev/sda1 /mnt
"""

    def _offline(self):
        # 离线安装
        # https://wiki.archlinux.org/index.php/Offline_installation
        return """
echo offline inst file ...
cp -ax / /mnt
cp -vaT /run/archiso/bootmnt/arch/boot/$(uname -m)/vmlinuz* /mnt/boot/vmlinuz-linux
# 进入新安装的环境执行
#arch-chroot /mnt
#arch-chroot /mnt /bin/bash

echo Restore the configuration of journald ...
sed -i 's/Storage=volatile/#Storage=auto/' /mnt/etc/systemd/journald.conf
echo Remove special udev rule...
rm -f /mnt/etc/udev/rules.d/81-dhcpcd.rules
echo Disable and remove the services created by archiso...
arch-chroot /mnt systemctl disable pacman-init.service choose-mirror.service
rm -rf /mnt/etc/systemd/system/{choose-mirror.service,pacman-init.service,etc-pacman.d-gnupg.mount,getty@tty1.service.d}
rm -f /mnt/etc/systemd/scripts/choose-mirror
echo Remove special scripts of the Live environment ...
rm -f /mnt/etc/systemd/system/getty@tty1.service.d/autologin.conf
rm -f /mnt/root/{.automated_script.sh,.zlogin}
rm -f /mnt/etc/mkinitcpio-archiso.conf
rm -rf /mnt/etc/initcpio
echo Importing archlinux keys...
arch-chroot /mnt pacman-key --init
arch-chroot /mnt pacman-key --populate archlinux
###############################################################################
"""

    def _fstab(self):
        # 安装完成后执行
        return """
echo genfstab ...
genfstab -U /mnt >> /mnt/etc/fstab
"""

    def _localtime(self):
        # 下面在新环境执行
        return """
echo localtime ...
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
"""

    def _hwclock(self):
        # 同步硬件时钟
        return """
echo hwclock ...
arch-chroot /mnt hwclock -w
"""

    def _hostname(self):
        # /etc/hosts
        return """
echo hostname ...
echo %s > /mnt/etc/hostname
""" % self.args.hostname

    def _locale(self):
        # 编辑本地化配置文件
        return """
echo locale-gen ...
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_CN.UTF-8/zh_CN.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_HK.UTF-8/zh_HK.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_SG.UTF-8/zh_SG.UTF-8/g' /mnt/etc/locale.gen
sed -i 's/#zh_TW.UTF-8/zh_TW.UTF-8/g' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

#arch-chroot /mnt echo LANG=en_US.UTF-8 > /etc/locale.conf # shell xxx
echo locale.conf ...
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf
"""

    def _initramfs(self):
        return """
# Initramfs
echo Initramfs ...
arch-chroot /mnt mkinitcpio -P
#安装 GRUB 软件包 只有上面不能正常引导
#arch-chroot /mnt pacman --noconfirm -S grub
arch-chroot /mnt grub-install --target=i386-pc --recheck /dev/sda
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg
"""

    def _root(self):
        return """
# Root password
echo arch-chroot /mnt passwd ！！
mkdir -p /mnt/root/.ssh
cat<<EOF>/mnt/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDwLa28qeq6OaFHaGtI7XexUBlw2tt0IOH00PHWsOGVrhwNfuKnxzrWPCXuHH4GI8qGtB7yf+OK73j8fRV0GL0pW3GefcjTqyPvGds2e3va+cbmr1dTFT6oabVb81Vi2iqyHDn7Jna9nPu/T0WcWEVhMh/JGESEdIVoQZYN18xHxhg9KnMRB1mBQvpSRtnorJLW5X+5X+2kFKZIpmKtA0EYnJ4zpaw1LqBSxRQ/yr+nKK+vuOCYpXAGYyjdxhdVNuAhSOQJuqSYG87qoJxTGjv1LnpnzqPqeBwZwI2ahC6JssUDC4mZl7iHc7RuEKMt/4hW+xSbaZIFH+Klssq4aSYd root@DESKTOP-JBAB3OE
EOF
chmod 0700 /mnt/root/.ssh
chmod 0600 /mnt/root/.ssh/authorized_keys
"""

    def _network(self):
        return """
# 配置网络，设置静态ip
echo config network
cat<<EOF>>/mnt/etc/dhcpcd.conf

interface %s
static ip_address=%s
static routers=192.168.56.1
static domain_name_servers=192.168.56.1

EOF
arch-chroot /mnt systemctl enable dhcpcd@%s
""" % (self.args.netif, self.args.ipaddr, self.args.netif)

    def _sshd(self):
        # 开启ssh登录服务
        return """
arch-chroot /mnt systemctl enable sshd
"""

    def _reboot(self):
        return """
# 重新启动
echo umount /mnt
echo reboot
"""


if __name__ == '__main__':
    self = InstallArchlinux()
    self.main()
