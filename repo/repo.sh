set -x
abs_path=$(readlink -f $0)
abs_path=${abs_path%/*}

ISO=$1
DEV=$2

ISO=${ISO:-/host_iso}
DEV=${DEV:-iso}

iso_path=$(readlink -f $ISO)
if (mount|grep ${iso_path} 2>/dev/null)then
  mount -t vboxsf ${DEV} ${iso_path}
fi

ISOS=($(ls ${abs_path}/|grep iso))
for iso in ${ISOS[@]};do
  if [ -f ${iso_path}/${iso} ]; then
    mount -B {${iso_path},${abs_path}}/${iso}
    [ -d ${abs_path}/${iso%.iso} ] && mount -o ro ${iso_path}/${iso} ${abs_path}/${iso%.iso}
  fi
done
