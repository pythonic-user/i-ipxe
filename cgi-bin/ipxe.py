#!/usr/bin/python

import os
import cgi

fields = cgi.FieldStorage()
#QUERY_STRING=os.environ.get("QUERY_STRING", "")
sno = fields.getvalue('sno', '')
uuid = fields.getvalue('uuid', '')

# ipxe/main.txt
path = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../'))

def head():
    print("200 ok")
    print("Content-Type: text/plain")
    print("")

print("")
if os.path.exists(os.path.join(path, 'hint', sno, 'menu.txt')):
    print(open(os.path.join(path, 'hint', sno, 'menu.txt')).read())
elif os.path.exists(os.path.join(path, 'hint', uuid, 'menu.txt')):
    print(open(os.path.join(path, 'hint', uuid, 'menu.txt')).read())
else:
    print(open(os.path.join(path, 'ipxe/main.txt')).read())
