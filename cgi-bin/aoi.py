#!/usr/bin/python
import traceback
import os
import cgi


# ipxe/main.txt
path = os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../'))

def head():
    print("200 ok")
    print("Content-Type: text/plain")
    print("")

print("")
try:
    text = open(os.path.join(path, 'ipxe/script/arch/offline_install.py')).read()
    net_seq = os.environ["REMOTE_ADDR"].rsplit(".", 1)[0]
    cmd = "ip addr|grep %s|awk '{print $2}'|awk -F / '{print $1}'" % net_seq
    ip = os.popen(cmd).read().strip()
    #print(text.count("'192.168.56.10'"), cmd,"'%s'"%ip)
    text = text.replace("'192.168.56.10'", "'%s'"%ip)
    print(text)
except:
    print(traceback.format_exc())
