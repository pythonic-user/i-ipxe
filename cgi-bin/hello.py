#!/usr/bin/env python
import os
import cgi, cgitb 

print( "200 ok")
print( "Content-Type: text/plain")
print( "")
print( "Hello World")
env = os.environ
for key in env:
    print("key=",key,"value=",env[key])

fields = cgi.FieldStorage() 
for key in fields:
    print("key=",key,"value=",fields[key].value)
